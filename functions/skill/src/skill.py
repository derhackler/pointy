# -*- coding: utf-8 -*-

import logging
import time
import hashlib

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.utils import is_request_type, is_intent_name

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import IntentConfirmationStatus, SlotConfirmationStatus
from ask_sdk_model.slu.entityresolution.status_code import StatusCode
from ask_sdk_model.dialog import DelegateDirective
from ask_sdk_core.serialize import DefaultSerializer

# custom modules
import functions.skill.src.mydb as mydb
import functions.skill.src.historyhelpers as historyhelpers

POINTS_SLOT = 'Points'
GRANTTYPE_SLOT = 'GrantType'
NAME_SLOT = 'Name'

sb = SkillBuilder()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


@sb.request_handler(can_handle_func=is_request_type("LaunchRequest"))
def launch_request_handler(handler_input):
    ''' Handler for Skill Launch'''

    user_id = _get_hashed_small_userid(handler_input)

    speech_text = "Willkommen bei Punktesammler."
    ask_text = "Sage zum Beispiel: gib Benjamin drei Punkte."

    logger.info('{} [LaunchIntent]'.format(user_id))

    return handler_input.response_builder \
                        .speak(speech_text) \
                        .ask(ask_text) \
                        .set_should_end_session(False) \
                        .response


@sb.request_handler(can_handle_func=is_intent_name("Reset"))
def reset_request_handler(handler_input):
    ''' Handler for Reset intent which deletes all data'''

    user_id = _get_hashed_small_userid(handler_input)
    current_intent = handler_input.request_envelope.request.intent
    confirmation_status = current_intent.confirmation_status

    # the user cancelled the request
    if confirmation_status == IntentConfirmationStatus.DENIED:
        logger.info('{} [ResetIntent] Canceled a reset request.'.format(user_id))
        text = 'Ok. Nichts passiert.'
        cardtitle = 'Abgebrochen'
        return handler_input.response_builder \
                            .speak(text) \
                            .set_card(SimpleCard(cardtitle, text)) \
                            .set_should_end_session(True) \
                            .response

    # the user confirmed the reset request
    elif confirmation_status == IntentConfirmationStatus.CONFIRMED:
        # deletes all data of the user
        logger.info('{} [ResetIntent] Confirmed reset.'.format(user_id))
        mydb.reset(user_id)

        text = 'Erledigt. Alle deine Daten wurden gelöscht.'
        cardtitle = 'Servus und Auf Wiedersehen'
        return handler_input.response_builder \
                            .speak(text) \
                            .set_card(SimpleCard(cardtitle, text)) \
                            .set_should_end_session(True) \
                            .response

    # the user stated the reset intent but didn't confirm it yet
    else:
        logger.debug('{} [ResetIntent] Confirmation needed'.format(user_id))
        return handler_input \
            .response_builder \
            .add_directive(DelegateDirective(updated_intent=current_intent)) \
            .set_should_end_session(False) \
            .response


@sb.request_handler(can_handle_func=is_intent_name('Overview'))
def overview_intent_handler(handler_input):
    ''' Handler for Overview intent which presents an overview of all points'''

    user_id = _get_hashed_small_userid(handler_input)

    history = mydb.get_history(user_id)

    text = ''
    cardtitel = 'Übersicht'

    if history:
        total = historyhelpers.summarize_history(history)
        text = historyhelpers.build_summary_text(total)
    else:
        text = 'Du hast noch niemandem Punkte gegeben.'

    logger.info('{} [OverviewIntent]'.format(user_id))

    return handler_input.response_builder \
                        .speak(text) \
                        .set_card(SimpleCard(title=cardtitel, content=text)) \
                        .set_should_end_session(True) \
                        .response


@sb.request_handler(can_handle_func=is_intent_name('OverviewForPerson'))
def overview_for_person_intent_handler(handler_input):
    ''' Handler for OverviewForPerson which presents all points for a given person '''

    current_intent = handler_input.request_envelope.request.intent
    slots = handler_input.request_envelope.request.intent.slots
    user_id = _get_hashed_small_userid(handler_input)

    name = ''
    grant_type = ''
    grant_type_label = ''

    if NAME_SLOT in slots:
        if slots[NAME_SLOT].value:
            name = slots[NAME_SLOT].value.capitalize()

    if GRANTTYPE_SLOT in slots:
        if slots[GRANTTYPE_SLOT].value:
            grant_type_label = slots[GRANTTYPE_SLOT].value.capitalize()
            grant_type = _get_grant_type(slots[GRANTTYPE_SLOT])

    # have alexa fill the required slots if possible
    if not (name and grant_type):
        logger.info('{} [OverviewForPersonIntent] Missing slot values.'.format(user_id))
        return handler_input \
            .response_builder \
            .add_directive(DelegateDirective(updated_intent=current_intent)) \
            .set_should_end_session(False) \
            .response

    history = mydb.get_history(user_id)
    total = historyhelpers.total_points(name, grant_type, history)

    text = ''
    cardtitel = 'Übersicht'
    if total != 0:
        text = '{} hat {} {}.'.format(name, total, grant_type_label)
    else:
        text = '{} hat keine {}'.format(name, grant_type_label)

    logger.info('{} [OverviewForPersonIntent] for "{}"'.format(user_id, name))

    return handler_input.response_builder \
                        .speak(text) \
                        .set_card(SimpleCard(title=cardtitel, content=text)) \
                        .set_should_end_session(True) \
                        .response


@sb.request_handler(
        can_handle_func=lambda handler_input:
        is_intent_name("GrantPoints")(handler_input) or
        is_intent_name("ReducePoints")(handler_input)
)
def grantpoints_intent_handler(handler_input):
    ''' Handler to grant or reduce points '''

    slots = handler_input.request_envelope.request.intent.slots
    user_id = _get_hashed_small_userid(handler_input)

    # multiplies the points by -1 if if the user wants to decrease the points
    intent = 'GrantPoints'
    multiplyer = 1
    if is_intent_name('ReducePoints')(handler_input):
        intent = 'ReducePoints'
        multiplyer = -1

    points = 0
    name = ''
    name_confirmed = False
    grant_type = ''
    grant_type_label = ''

    if POINTS_SLOT in slots:
        points = slots[POINTS_SLOT].value

    if NAME_SLOT in slots:
        if slots[NAME_SLOT].value:
            name = slots[NAME_SLOT].value.capitalize()
            name_confirmed = slots[NAME_SLOT].confirmation_status

    if GRANTTYPE_SLOT in slots:
        if slots[GRANTTYPE_SLOT].value:
            grant_type_label = slots[GRANTTYPE_SLOT].value.capitalize()
            grant_type = _get_grant_type(slots[GRANTTYPE_SLOT])

    # have alexa fill the required slots if not all are given
    if not (name and points and grant_type):
        # I need to make sure to set the granttype to either none or a valid value
        # Otherwise a value such as "mygrant" would be accepted for grant type
        slots[GRANTTYPE_SLOT].value = grant_type
        return _ask_for_missing_slots(handler_input)


    # if the name is not confirmed yet, check with existing names for similarity
    history = mydb.get_history(user_id)
    is_newname = historyhelpers.is_a_new_name(history, name)
    if name_confirmed == SlotConfirmationStatus.NONE:
        if is_newname:
            return _ask_for_confirmation_of_the_name(handler_input)
    elif name_confirmed == SlotConfirmationStatus.DENIED:
        return _no_response(handler_input)
    else: # SlotConfirmationStatus.CONFIRMED
        pass
    
    name = historyhelpers.get_existing_name_that_sounds_the_same(history, name, default=name)

    # creating the item that will be added to the history
    item = {
        'timestamp':            int(time.time()),
        'grant_type':           grant_type,
        'grant_type_label':     grant_type_label,
        'name':                 name,
        'points':               int(float(points)) * multiplyer
    }

    # persist the event in the db
    history = mydb.store_change(user_id, item)

    # Sum it up
    total = historyhelpers.total_points(name, grant_type, history)

    logger.info('{} [{}Intent] for: "{}" what: "{}" whatlabel: "{}" howmany: "{}"'.format(user_id, intent, name, grant_type, grant_type_label, points))

    # Final outcome
    text = ''
    cardtitle = ''
    if intent == 'GrantPoints':
        text = 'Du hast {} {} {} gegeben. '.format(name, points, grant_type_label)
        text += '{} hat jetzt {} {}.'.format(name, total, grant_type_label)
        cardtitle = '{} vergeben'.format(grant_type_label)
    else:
        text = 'Du hast {} {} {} abgezogen. '.format(name, points, grant_type_label)
        text += '{} hat jetzt {} {}.'.format(name, total, grant_type_label)
        cardtitle = '{} vergeben'.format(grant_type_label)

    return handler_input.response_builder \
                        .speak(text) \
                        .set_card(SimpleCard(cardtitle, text)) \
                        .set_should_end_session(True) \
                        .response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.HelpIntent"))
def help_intent_handler(handler_input):
    ''' Handler for Help Intent. '''

    user_id = _get_hashed_small_userid(handler_input)

    speech_text = "Gib einer Person Punkte, Sterne, oder Minuten oder zieh sie ihr wieder ab."
    speech_text += " Du kannst auch den aktuellen Punktestand abfragen. Was möchtest du machen?"
    ask = "Sage zum Beispiel: gib Benjamin drei Punkte."

    logger.info('{} [HelpIntent] '.format(user_id))

    return handler_input.response_builder \
                        .speak(speech_text) \
                        .ask(ask) \
                        .set_card(SimpleCard("Hilfe", speech_text)) \
                        .set_should_end_session(False) \
                        .response


@sb.request_handler(
    can_handle_func=lambda handler_input:
        is_intent_name("AMAZON.CancelIntent")(handler_input) or
        is_intent_name("AMAZON.StopIntent")(handler_input) or
        is_intent_name("Cancel")(handler_input))
def cancel_and_stop_intent_handler(handler_input):
    """Single handler for Cancel and Stop Intent."""
    # type: (HandlerInput) -> Response
    user_id = _get_hashed_small_userid(handler_input)
    
    logger.info('{} [StopIntent]'.format(user_id))

    speech_text = "Bis bald!"
    return handler_input.response_builder \
                    .speak(speech_text) \
                    .set_card(SimpleCard("Bis bald", speech_text)) \
                    .set_should_end_session(True) \
                    .response


@sb.request_handler(can_handle_func=is_intent_name("AMAZON.FallbackIntent"))
def fallback_handler(handler_input):
    """AMAZON.FallbackIntent is only available in en-US locale.
    This handler will not be triggered except in that locale,
    so it is safe to deploy on any locale.
    """
    # type: (HandlerInput) -> Response
    user_id = _get_hashed_small_userid(handler_input)
    logger.info('{} [FallbackIntent] handler_input: {}'.format(user_id, handler_input))

    speech = "Sag 'Hilfe' wenn du nicht weiter weißt."
    reprompt = "Du kannst 'Hilfe' sagen."
    handler_input.response_builder.speak(speech).ask(reprompt)
    return handler_input.response_builder.response


@sb.request_handler(can_handle_func=is_request_type("SessionEndedRequest"))
def session_ended_request_handler(handler_input):
    """Handler for Session End."""
    # type: (HandlerInput) -> Response
    return handler_input.response_builder.response


@sb.exception_handler(can_handle_func=lambda i, e: True)
def all_exception_handler(handler_input, exception):
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    # type: (HandlerInput, Exception) -> Response
    user_id = _get_hashed_small_userid(handler_input)

    logger.error('{} [Error] handler_input: {}'.format(user_id, handler_input.request_envelope.request.to_str()))
    logger.error(exception, exc_info=True)

    text = "Irgendwas hat nicht funktioniert. Bitte versuchs nochmal!"
    ask = 'Was möchtest du machen?'
    return handler_input.response_builder \
                        .speak(text) \
                        .ask(ask) \
                        .set_card(SimpleCard("Hilfe", text)) \
                        .set_should_end_session(True) \
                        .response


def _no_response(handler_input):
    return handler_input \
        .response_builder \
        .set_should_end_session(True) \
        .response


def _ask_for_missing_slots(handler_input):
    current_intent = handler_input.request_envelope.request.intent
    user_id = _get_hashed_small_userid(handler_input)

    logger.info('{} [{}Intent] Ask for missing slot values.'.format(user_id, current_intent.name))
    return handler_input \
        .response_builder \
        .add_directive(DelegateDirective(updated_intent=current_intent)) \
        .set_should_end_session(False) \
        .response


def _ask_for_confirmation_of_the_name(handler_input):
    current_intent = handler_input.request_envelope.request.intent
    user_id = _get_hashed_small_userid(handler_input)
    slots = handler_input.request_envelope.request.intent.slots

    logger.info('{} [{}Intent] Ask for confirmation of new name: "{}"'.format(user_id, current_intent.name, slots[NAME_SLOT].value))
    return handler_input \
        .response_builder \
        .add_directive(DelegateDirective(updated_intent=current_intent)) \
        .set_should_end_session(False) \
        .response


def _get_grant_type(slot):
    if not slot.resolutions:
        return slot.value

    resolution = slot.resolutions.resolutions_per_authority[0]
    if resolution.status.code == StatusCode.ER_SUCCESS_NO_MATCH:
        return ""
    else:
        return resolution.values[0].value.id


def _get_hashed_small_userid(handler_input):
    user_id = handler_input.request_envelope.context.system.user.user_id
    return hashlib.sha256(user_id.encode()).hexdigest()[0:40]


handler = sb.lambda_handler()