import boto3
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

dynamodb = boto3.resource('dynamodb', region_name="eu-west-1")


def reset(user_id):
    tablename = os.environ['POINTS_TABLE_NAME']
    table = dynamodb.Table(tablename)
    table.delete_item(
        TableName=tablename,
        Key={
            'id': user_id,
        }
    )


def get_history(user_id):
    table = dynamodb.Table(os.environ['POINTS_TABLE_NAME'])
    response = table.get_item(
        Key={
            'id': user_id,
        }
    )

    if 'Item' in response:
        return response['Item']['history']
    else:
        return []


def store_change(user_id, points_change):
    table = dynamodb.Table(os.environ['POINTS_TABLE_NAME'])
    response = table.update_item(
        Key={
            'id': user_id,
        },
        UpdateExpression='set history = list_append(if_not_exists(history, :empty_list), :h)',
        ExpressionAttributeValues={
            ':h': [points_change],
            ':empty_list': [],
        },
        ReturnValues="UPDATED_NEW"
    )
    return response['Attributes']['history']
