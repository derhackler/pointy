from functools import reduce
import functions.skill.src.koelnphonetics as koelnphonetics

def used_names(history):
    '''
    returns ['name1','name2']
    '''
    mapped = [item['name'] for item in history]
    return list(set(mapped)) # to deduplicate the names


def summarize_history(history):
    '''
    returns {'name':
                { 'grant_type':
                    { 'grant_type_label': 'label','points': 5 }}}
    '''
    summary = {}
    for i in history:
        new = {
                'grant_type_label':     i['grant_type_label'],
                'points':               i['points']
            }
        current = summary.get(i['name'], {})
        if not current:         # first occurance of a name
            summary[i['name']] = {i['grant_type']: new}
        else:
            points = current.get(i['grant_type'], {})
            if not points:      # first occurance of a specific grant type
                summary[i['name']][i['grant_type']] = new
            else:
                summary[i['name']][i['grant_type']]['points'] += i['points']
                summary[i['name']][i['grant_type']]['grant_type_label'] = i['grant_type_label']

    return summary


def total_points(name, grant_type, history):
    mapped = [item['points'] for item in history if (item['name'] == name and item['grant_type'] == grant_type)]
    return reduce((lambda x, y: x+y), mapped, 0)


def build_summary_text(summary):
    text = ''
    for person, grant_type_dict in summary.items():
        text += '{} hat '.format(person)
        for i, grant_type in enumerate(grant_type_dict):
            points = grant_type_dict[grant_type]['points']
            label = grant_type_dict[grant_type]['grant_type_label']
            if i == 0:
                text += '{} {}'.format(points, label)
                if i == (len(grant_type_dict) - 1):
                    text += '. '
            elif i+1 < len(grant_type_dict):
                text += ', {} {}'.format(points, label)
            else:
                text += ' und {} {}. '.format(points, label)
    if text == '':
        text = 'Du hast noch niemandem Punkte, Sternchen, oder Minuten gegeben.'
    return text


def is_a_new_name(history, name):
    return not get_existing_name_that_sounds_the_same(history, name)


def get_existing_name_that_sounds_the_same(history, name, default=None):
    allused = used_names(history)
    for used in allused:
        if koelnphonetics.compare(used,name):
            return used

    return default