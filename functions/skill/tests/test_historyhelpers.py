import functions.skill.src.historyhelpers as hh
import re

def test_hello():
    assert 1==1

def test_summarize_empty_history():
    history = []
    assert hh.summarize_history(history) == {}

def test_summarize_history():
    history = []
    history.append(hist('n1','g1','gl',1))
    history.append(hist('n2','g1','gl',5))
    history.append(hist('n1','g1','gl',1))
    history.append(hist('n2','g2','g2l',5))
    history.append(hist('n1','g1','gl',-3))

    s = hh.summarize_history(history)
    assert s['n1']['g1']['points'] == -1
    assert s['n2']['g1']['points'] == 5
    assert s['n2']['g2']['points'] == 5
    
def hist(name, grant_type, grant_type_label, points):
    return {
        'name': name,
        'grant_type': grant_type,
        'grant_type_label': grant_type_label,
        'points': points
    }

def test_empty_summary_text():
    test_summary = {}
    text = hh.build_summary_text(test_summary)
    assert re.search('noch niemandem',text)

def test_complex_summary_text():
    summary = test_summary()
    text = hh.build_summary_text(summary)
    assert re.search('adam hat 5 points, 3 stars und 2 minutes.', text)
    assert re.search('bob hat 50 points und 30 stars.', text)
    assert re.search('charlie hat 500 points.', text)

def test_summary():
    history = {
        'adam': {
            'POINTS': {
                'grant_type_label': 'points',
                'points': 5
            },
            'STARS': {
                'grant_type_label': 'stars',
                'points': 3
            },
            'MINUTES': {
                'grant_type_label': 'minutes',
                'points': 2
            },
        },
        'bob': {
            'POINTS': {
                'grant_type_label': 'points',
                'points': 50
            },
            'STARS': {
                'grant_type_label': 'stars',
                'points': 30
            },
        },
        'charlie': {
            'POINTS': {
                'grant_type_label': 'points',
                'points': 500
            },
        }
    }

    return history
