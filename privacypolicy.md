Datenschutzvereinbarung
=======================
Eine Nutzung des Pünktchen Skills für Amazon Alexa ist grundsätzlich ohne Angabe personenbezogener Daten möglich. Sofern Sie echte Vornamen verwenden, verarbeiten wir personenbezogene Daten.

Mit diesem Hinweis informieren wir Sie darüber, welche Daten wir von Ihnen erheben, wie wir sie nutzen und wie Sie der Datennutzung widersprechen können.

Wer ist verantwortlich
----------------------
Benedikt Eckhard (public@derhackler.at) erhebt und verarbeitet Ihre Daten als Verantwortlicher. Sollten Sie Fragen oder Anregungen zum Datenschutz haben, kontaktieren Sie bitte ihn.

Welche Daten werden erhoben
---------------------------
Wir erheben und verarbeiten Ihre Daten ausschließlich zu bestimmten Zwecken. Diese können sich aus technischen Notwendigkeiten oder ausdrücklichen Nutzerwünschen ergeben.

Daten, die aufgrund von ausdrücklichen Nutzerwünschen im Pünktchen Skill gespeichert werden, sind:

- Verwendete Vornamen
- Historie wann und wie viele Punkte Sie welchem Vornamen zugeornet haben

Aus technischen Gründen müssen wir bei der Nutzung des Pünktchen Skills für Amazon Alexa bestimmte Daten erheben und und in Form von Logdateien speichern.

Diese betrifft im Einzelnen:

- Ihre Alexa UserID
- Datum und Dauer der Nutzung
- Anfragen an den Pünktchen Skill
- Antworten des Pünktchen Skills
- Fehlermeldung bei der Nutzung des Pünktchen Skills

Konkret erfolgt die Datenverarbeitung folgendermaßen:

Nach Ihrer Aktivierung des Pünktchen Skills in der Alexa App können Sie Alexa den Sprachbefehl geben, eine Frage an unseren Informationsdienst oder eine sonstige Aufforderung an uns zu übermitteln. Zur Ausführung dieses Befehls wird uns Amazon die folgenden Daten weiterleiten:

- Die von Amazon Ihnen bei Aktivierung des Pünktchen Skills zugewiesene individuelle “UserID”. Sie ist für den Skill eindeutig und kann nicht mit anderen verwendeten Skills korreliert werden.
- Die von Ihnen in Bezug auf den Pünktchen Skill gestellte Frage oder Aufforderung

Soweit wir eine passende Antwort auf Ihre uns übermittelte Frage oder Aufforderung haben, geben wir diese in Textform an Amazon weiter. Im Rahmen von Alexa erfolgt sodann die Umwandlung des Textes in eine akustische Sprachausgabe, die Ihnen über Ihr Alexafähiges Gerät abgespielt wird. Die Art und Weise der Verarbeitung Ihrer personenbezogenen Daten bevor eine Ihrer Fragen oder Aufforderungen von Amazon an uns übermittelt wurde, sowie nachdem wir eine entsprechende Antwort für Sie an Amazon übermittelt haben, liegt außerhalb unseres Einfluss- und Verantwortungsbereichs. Die von uns zu verantwortende Verarbeitung Ihrer personenbezogenen Daten beginnt mit dem Erhalt Ihrer personenbezogenen Daten durch Übermittlung von Amazon an uns und endet, wenn wir eine entsprechende Antwort in Textform an Amazon zur Übermittlung an Sie übergeben haben.

Ihre UserID und alle damit vergebenen Punkte, werden für spätere Fragen oder Aufforderungen so lange gespeichert, wie der Pünktchen Skill in Alexa für Sie aktiviert ist.

Soweit wir in dem vorstehend beschriebenen Umfang Ihre Daten verarbeiten, sind wir hierzu gemäß Art 6 Abs. 1 lit. b) DSGVO berechtigt, da dies zur Erfüllung Ihrer Anfrage zwingend erforderlich ist.

Wie lange werden ihre Daten gespeichert
---------------------------------------
Wir speichern Ihre Daten nur so lange, wie sie für die Erfüllung des Zwecks, zu dem sie erhoben wurden, erforderlich sind oder sofern dies gesetzlich vorgesehen ist. So speichern wir im Rahmen eines Vertragsverhältnisses Ihre Daten mindestens bis zur vollständigen Beendigung des Vertrages. Anschließend werden die Daten für die Dauer der gesetzlichen Aufbewahrungsfristen aufbewahrt. Die aus technischen Gründen (Datum und Dauer der Nutzung, Anfragen an den Pünktchen Skill, Fehlermeldungen) gespeicherten Daten werden nach 35 Tagen gelöscht.

Die gespeicherten Daten können Sie zudem jederzeit löschen, indem Sie im Pünktchen Skill "Reset" oder "Meine Daten löschen" sagen. Ihre gespeicherten Daten werden des Weiteren gelöscht, wenn Sie den Pünktchen Skill deaktivieren.

Werden Daten weitergegeben?
---------------------------
Wenn Sie den Pünktchen Skill nutzen, werden Ihre personenbezogenen Daten über Alexa-fähige Endgeräte auch an Amazon weitergegeben. Auf eine etwaige Datenverarbeitung Ihrer personenbezogenen Daten durch Amazon haben wir keinen Einfluss. Zum Umgang mit Ihren personenbezogenen Daten bei Amazon besuchen Sie bitte die Datenschutzerklärung von Amazon unter folgender Adresse: https://www.amazon.de/gp/help/customer/display.html?nodeId=3312401

Für die von uns verantwortete Datenverarbeitung gilt, dass zur Abwicklung des von uns angeforderten Dienstes in der Regel die Einschaltung weisungsabhängiger Auftragsverarbeiter erforderlich ist, wie z. B. von Rechenzentrum-Betreibern. Externe Dienstleister, die für uns im Auftrag Daten verarbeiten, werden von uns sorgfältig ausgewählt und vertraglich streng verpflichtet. Die Dienstleister arbeiten nach unserer Weisung, was durch strenge vertragliche Regelungen, durch technische und organisatorische Maßnahmen und durch ergänzende Kontrollen sichergestellt wird.

Eine Übermittlung Ihrer Daten erfolgt im Übrigen nur, wenn Sie uns dazu eine ausdrückliche Einwilligung erteilt haben oder aufgrund einer gesetzlichen Regelung.

Eine Übermittlung in Drittstaaten außerhalb der EU/des EWR oder an eine internationale Organisation findet durch uns nicht statt, es sei denn, es liegen angemessene Garantien vor. Dazu gehören die EU-Standardvertragsklauseln sowie ein Angemessenheitsbeschluss der EU-Kommission.

Findet eine Analyse der Skill-Nutzung statt?
--------------------------------------------
Um Ihr Nutzungserlebnis kontinuierlich zu verbessern, erheben wir Statistiken über die Nutzung des Pünktchen Skills. Hierfür nutzen wir die integrierten Analyse-Tools der Alexa Developer Console (developer.amazon.com/alexa). Die im Folgenden aufgeführten und von uns eingesetzten Tracking-Maßnahmen werden auf Grundlage des Art. 6 Abs. 1 lit. f) DSGVO durchgeführt. Mit den zum Einsatz kommenden Tracking-Maßnahmen wollen wir eine bedarfsgerechte Gestaltung und die fortlaufende Optimierung unseres Skills sicherstellen. Zum anderen setzen wir die Tracking-Maßnahmen ein, um die Nutzung des Pünktchen Skills statistisch zu erfassen und zum Zwecke der Optimierung unseres Angebotes für Sie auszuwerten. Diese Interessen sind als berechtigt im Sinne der vorgenannten Vorschrift anzusehen.

In der Amazon Alexa Development Konsole können wir sehen, welche Funktionen im Pünktchen Skill wie oft genutzt werden und ob die Gestaltung des Skills einen Einfluss auf den Umfang der Nutzung hat.

Über die gewonnenen Statistiken können wir unser Angebot verbessern und für Sie als Nutzer interessanter ausgestalten.

Diese Informationen werden von Amazon an uns ausschließlich als kumulierte Daten weitergegeben, die die allgemeine Skill-Nutzung darstellen. Diese Daten sind nicht personenbezogen oder auf eine Person zurückzuführen.

Dieses Tracking können Sie nicht deaktivieren. Wenn Sie das stört, deaktiveren sie den Pünktchen Skill bitte.

Welche Rechte haben NutzerInnen des Pünktchen Skills?
-----------------------------------------------------
- Sie können Auskunft darüber verlangen, welche Daten über Sie gespeichert sind.
- Sie können Berichtigung, Löschung und Einschränkung der Bearbeitung (Sperrung) ihrer personenbezogenen Daten verlangen, solange dies gesetzlich zulässig und im Rahmen eines bestehenden Vertragsverhältnisses möglich ist.
- Sie haben das Recht, Beschwerde bei einer Aufsichtsbehörde einzulegen.
- Sie können der Datenverarbeitung aus Gründen, die sich aus Ihrer besonderen Situation ergeben, widersprechen, wenn die Datenverarbeitung aufgrund unserer berechtigten Interessen erfolgt.

Für die Ausübung Ihrer Rechte schicken sie bitte ein Email an public@derhackler.at

Aktualisierung des Datenschutzhinweises
---------------------------------------
Wir passen den Datenschutzhinweis an veränderte Funktionalitäten oder geänderte Rechtslagen an. Daher empfehlen wir, den Datenschutzhinweis in regelmäßigen Abständen zur Kenntnis zu nehmen. Sofern Ihre Einwilligung erforderlich ist oder Bestandteile des Datenschutzhinweises Regelungen des Vertragsverhältnisses mit Ihnen enthalten, erfolgen die Änderungen nur mit Ihrer Zustimmung.

Stand Jänner 2019

Sonstiges
---------
Diese Datenschutzvereinbarung wurde inspiriert von der sehr gut gestalteten Vereinbarung des Deutsche Bahn Skill für Amazon Alexa.