# Punktesammler (Pointy)
> An Alexa skill to track points

Pointy is a (currently) German only Alexa skill that tracks points (or stars, or minutes).
The original intent was that you can grant your kids points if they did something
great (like letting you finish debugging your Alexa skill) and exchange those points to
iphone minutes.

## Installation

* You have to have an Alexa that is configured to speak German.
* Install the skill "Punktesammler" to try it out

## Usage examples

Unfortunately in German only

### Grant a person points

- "Alexa, öffne punktesammler und gib Elisabeth fünf Punkte"
- "Alexa, öffne punktesammler und gib Benjamin drei Sternchen"

### Get an overview

- "Alexa, frage punktesammler wie viele Sterne Benjamin hat"
- "Alexa, frage punktesammler nach einer Punktübersicht"

### Deduce points

- "Alexa, öffne punktesammler"
- "Minus zwei Punkte für Elisabeth"
- "Alexa, öffnet punktesammler und löse zwei Sternchen von Benjamin ein"

### Drop all data

- "Alexa, öffne punktesammler"
- "Alle Daten löschen"


## Development setup

### Prerequisites

* Python 3.7
* The Servless framework (https://serverless.com)
* An AWS account
* An Alexa developer account
* ask-cli installed locally

### Building the layer
Create a new virtualenv.

Build the deploy artifact for the layer:

```
pushd layers/asksdk && chmod +x get_layers_package.sh && ./get_layers_package.sh && popd
```

### Deploying
`sls deploy` for the full deployment
`sls deploy -f skill` for deploying only the function


*TODO how to setup the skill model*
*TODO how to run the tests*

## Release History

* 1.0.0
    * First release

## Meta

Benedikt Eckhard – [@derhackler](https://twitter.com/derhackler) – public@derhackler.at

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/derhackler/catexcel](https://github.com/derhackler/catexcel)

## Contributing

1. Fork it (<https://github.com/derhackler/catexcel/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request






Pünktchen
=========
Pünktchen is an Alexa skill that allows tracking of points. It was originally built as spare time project by Benedikt Eckhard.

How to get it up and running?
--------------------------
- Install the serverless framework
- Log the serverless framework into an Amazon Account
- Create a "pointy_env" virtual env
- Refresh the PIP packages
- Deploy it with `serverless deploy`
- Deploy just the function with `serverless deploy -f skill` 

How to deploy to production?
----------------------------
- Change the environment to `prod` in the serverless.yml file
- Deploz with `serverless deploy`

Data store
----------
The data is stored in an Amazon Dynamo DB that is created during setup.

```
Schema:

- id (partition key): alexa device id
- history[]:
-- date: a unix timestamp
-- name: who got the points
-- grant_type: what for
-- grant_type_label: how as the grant type referred to by users
-- points: the number of points
```

Deutsche Beschreibung
---------------------
Auch wenn manche Pädagogen die Nase rümpfen: Kinder für gute Taten mit Punkten, Sternchen, oder Minuten zu belohnen, welche dann wieder gegen etwas eingetauscht werden können (z.B. Tablet schauen), kann für Kinder durchaus motivierend sein und den Erziehungsalltag von Eltern ein bisschen entspannen. Du kannst punktesammler natürlich auch zum Merken deines Spiele Punktestands, oder zum Zählen der von dir entdeckten Sterne verwenden.

Mit punktesammler kannst du ohne Setup gleich loslegen:

Du kannst beliebigen Personen Punkte, Sterne, oder Minuten geben:

- "Alexa, öffne punktesammler und gib Elisabeth fünf Punkte"
- "Alexa, öffne punktesammler und gib Benjamin drei Sternchen"

Du kannst eine Übersicht bekommen:

- "Alexa, frage punktesammler wie viele Sterne Benjamin hat"
- "Alexa, frage punktesammler nach einer Punktübersicht"

Du kannst Punkte wieder abziehen:

- "Alexa, öffne punktesammler"
- "Minus zwei Punkte für Elisabeth"
- "Alexa, öffnet punktesammler und löse zwei Sternchen von Benjamin ein"

Und du kannst jederzeit alle deine Daten löschen:

- "Alexa, öffne punktesammler"
- "Alle Daten löschen"

Viel Spaß mit punktesammler!


Für Entwickler: Der Skill ist Open Source: https://bitbucket.org/derhackler/pointy/src/master/