import json
from functools import reduce


def to_file(json_object, filename):
    with open(filename, 'w') as f:
        json.dump(json_object, f, indent=4, separators=(',', ': '), sort_keys=True)
        f.write('\n')


def deep_get(dictionary, keys, default=None):
    # from https://stackoverflow.com/questions/25833613/python-safe-method-to-get-value-of-nested-dictionary
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)