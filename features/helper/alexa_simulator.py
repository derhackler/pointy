import requests
import time


class AskResponse:
    def __init__(self, response):
        self.response = response
        self.is_error = False

        if response.get('status') == 'FAILED':
            self.is_error = True
            self.error_message = response.get('result').get('error').get('message')
        elif response.get('status') == 'SUCCESSFUL':
            self.speech = self._get_speech_response(response)

    def _get_speech_response(self, response):
        y = [x.get('content').get('caption')
             for x in response.get('result').get('alexaExecutionInfo').get('alexaResponses')
             if x['type'] == 'Speech']
        return y[0]


class AlexaSimulator:
    def __init__(self,
                 access_token,  # the amazon developer access token.
                 skill_id,
                 stage='development',
                 locale='de-DE'):
        self.access_token = access_token
        self.skill_id = skill_id
        self.stage = stage
        self.start_new_dialog = True
        self.locale = locale
        self.base_url = 'https://api.amazonalexa.com/v2'

    def say(self, say):
        r = requests.post(self._endpoint_url(),
                          headers=self._headers(),
                          json=self._say_body(say))

        r.raise_for_status()  # raises if the status code is not 200
        self.start_new_dialog = False  # so that the conversation can continue
        resp = r.json()

        # reponse looks like this:
        # Body:
        #   {
        #    "id": "34b1789...456ab",
        #    "status": "IN_PROGRESS",
        #    "result": null
        #    }
        wait_url = self._endpoint_url() + '/' + resp['id']
        return self._wait_and_return_answer(wait_url)

    def restart(self):
        self.start_new_dialog = True

    def _wait_and_return_answer(self, wait_url, try_nr=1):
        self._raise_after_n_tries(10, try_nr)
        time.sleep(0.2 * try_nr)  # incremental back off

        r = requests.get(wait_url, headers=self._headers())
        r.raise_for_status()

        resp = r.json()
        if resp['status'] == 'IN_PROGRESS':
            return self._wait_and_return_answer(wait_url, try_nr + 1)
        else:
            return AskResponse(resp)

    def _raise_after_n_tries(self, n, try_nr):
        if try_nr > n:
            raise "Didn't get a response after n tries"
        return

    def _endpoint_url(self):
        return f'{self.base_url}/skills/{self.skill_id}/stages/{self.stage}/simulations'  # noqa: E501

    def _headers(self):
        return {
            'Authorization': self.access_token,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

    def _say_body(self, say):
        return {
            "session": {
                "mode": self._session_mode()
            },
            "input": {
                "content": say
            },
            "device": {
                "locale": self.locale
            }
        }

    def _session_mode(self):
        if self.start_new_dialog:
            return 'FORCE_NEW_SESSION'
        else:
            return 'DEFAULT'
