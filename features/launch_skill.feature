Feature: Launch Skill

    Scenario: A launched skill waits for instructions
        When I say "Öffne Punktesammler"
        Then I hear "Willkommen bei Punktesammler"
        Then Alexa is listening

    Scenario: I can stop a launch request
        When I say "Öffne Punktesammler"
        And I say "stop"
        Then Alexa is not listening

    Scenario: I can stop a launch request
        When I say "Öffne Punktesammler"
        And I say "stopp"
        Then Alexa is not listening