from behave import given, when, then
from assertpy import assert_that


@given('I am a new user')
def step_impl(context):
    context.simulator.say("öffne punktesammler und lösche alle daten")
    # we have to confirm the intent
    context.simulator.say("ja")
    context.simulator.restart()

