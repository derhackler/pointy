from behave import given, when, then
from assertpy import assert_that
from features.helper.utils import to_file, deep_get
import time


@when('I say "{say}"')
def step_impl(context, say):
    context.response = context.simulator.say(say)
    # to_file(context.response.response, f"recording.{time.time()}.json")


@then('I hear "{hear}"')  # noqa: F811
def step_impl(context, hear):
    assert_that(context.response.is_error).is_false()
    assert_that(context.response.speech).contains_ignoring_case(hear)


@then('I hear one of this')  # noqa: F811
def step_impl(context):
    assert_that(context.response.is_error).is_false()

    heard = False
    for row in context.table:
        if row[0].lower() in context.response.speech.lower():
            heard = True

    assert heard, f"alexa said this: {context.response.speech}"


@then('Alexa is listening')  # noqa: F811
def step_impl(context):
    assert_that(context.response.is_error).is_false()
    assert_that(_ends_session(context.response.response)).described_as('shouldEndSession').is_false()


@then('Alexa is not listening')  # noqa: F811
def step_impl(context):
    assert_that(context.response.is_error).is_false()
    assert_that(_ends_session(context.response.response)).described_as('shouldEndSession').is_true()


def _ends_session(response):
    path = 'result.skillExecutionInfo.invocations'
    invocations = deep_get(response, path)[0]

    path = 'invocationResponse.body.response.shouldEndSession'
    return deep_get(invocations, path, True)