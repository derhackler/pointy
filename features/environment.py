from features.helper.alexa_simulator import AlexaSimulator
import os


def before_scenario(context, scenario):
    token = os.environ['AWS_ACCESS_TOKEN']
    skillid = os.environ['SKILL_ID']

    context.simulator = AlexaSimulator(token, skillid)