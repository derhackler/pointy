Feature: Granting Points

    Scenario: Points are added to existing points
        Given I am a new user
        When I say "öffne punktesammler und gib elisabeth drei punkte"
        # I get asked for confirmation, because elisabeth is new so I have to say "ja"
        And I say "ja"
        And I say "öffne punktesammler und gib elisabeth drei punkte"
        Then I hear "Elisabeth hat jetzt 6 punkte"


    Scenario: New names need to be confirmed
        Given I am a new user
        When I say "öffne punktesammler und gib elisabeth drei punkte"
        Then I hear one of this:
            | examples |
            | Möchtest du elisabeth wirklich drei punkte geben? |
            | Du hast elisabeth noch nie punkte gegeben. |
            | Elisabeth kenne ich noch nicht |
        When I say "ja"
        Then I hear "Elisabeth hat jetzt 3 punkte"


    Scenario: Similar names are recognized as one
        Given I am a new user
        When I say "öffne punktesammler und gib elisabeth drei punkte"
        # confirm because elisabeth is new
        And I say "ja"
        And I say "öffne punktesammler und gib elisabet drei punkte"
        Then I hear "Elisabeth hat jetzt 6 punkte"